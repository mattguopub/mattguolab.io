# Summary

* [Introduction](README.md)
* More

## mermaid

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

## math

Euler's formula is remarkable: $`e^{i\pi} + 1 = 0`$.
```math
e^{i\pi} + 1 = 0
```

## code

```java
class foo {
    publc static int bar = 42;
}
```
